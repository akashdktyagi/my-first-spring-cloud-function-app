package com.automationfraternity;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@SpringBootApplication
@Log4j2
public class CloudFunctionApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudFunctionApplication.class, args);
    }

    @Bean
    public Supplier<String> hello(){
        return ()-> "Hello World";
    }

    @Bean
    public Function<String, String> helloName() {
        return (name)-> "Hello '" + name + "'";
    }

    @Bean
    public Consumer<String> helloJustLog() {
        return (name)-> log.info("Hello '" + name + "'");
    }
}