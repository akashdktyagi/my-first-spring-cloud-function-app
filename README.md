# my-first-spring-cloud-function-app

* Go here for full details: https://www.linkedin.com/pulse/go-serverless-run-spring-cloud-function-app-aws-lambda-akash-tyagi


## What did I do to get started with my first spring cloud app on AWS Lambda

* For detailed info check here: https://cloud.spring.io/spring-cloud-function/reference/html/spring-cloud-function.html
* Use Spring Boot initizer and add these two dependencies, spring boot web and spring cloud function
    * ```xml
          <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-function-web</artifactId>
        </dependency>
        ```
* Add three methods in your Cloud Function Application
    * ```java

          @Bean
          public Supplier<String> hello(){
          return ()-> "Hello World";
          }
        
          @Bean
          public Function<String, String> helloName() {
          return (name)-> "Hello '" + name + "'";
          }
        
          @Bean
          public Consumer<String> helloJustLog() {
          return (name)-> log.info("Hello '" + name + "'");
          }
      ```
* What's important here is the return type of these methods. Spring Cloud function app utilizes these functional interface available since the Java 8
    * Supplier<T> : In java a suplier Returns something. So, automatically gets mapped to "GET" request.
    * Consumer<T>: As name suggests, Consumes things. So, automatically gets mapped to a "POST" request.
    * Function<I,O>: Does both, accepts and returns, can either be GET or POST or other.
    
* Each of the above method is marked as "@Bean". This tells the Spring Cloud Function app, to include these methods as https End points
* Run the app locally, and you will see below results:
    * ```shell
    
            curl -X GET http://localhost:8084/hello
            Hello World
            curl -X GET http://localhost:8084/helloName/Akash
            Hello 'Akash'                                                                                                                                                                                             
            curl -X GET http://localhost:8084/helloJustLog/Akash
    
        ```
      
* Build the shaded jar. A shaded jar is a file which has all the dependecies in put together in the same jar.
* Add below, snipet in the pom.xml
    * 
    ```xml
        <build>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <configuration>
                        <skip>true</skip>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <dependencies>
                        <dependency>
                            <groupId>org.springframework.boot.experimental</groupId>
                            <artifactId>spring-boot-thin-layout</artifactId>
                            <version>1.0.28.RELEASE</version>
                        </dependency>
                    </dependencies>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-shade-plugin</artifactId>
                    <version>3.2.4</version>
                    <configuration>
                        <createDependencyReducedPom>false</createDependencyReducedPom>
                        <shadedArtifactAttached>true</shadedArtifactAttached>
                        <shadedClassifierName>aws</shadedClassifierName>
                    </configuration>
                </plugin>
            </plugins>
        </build>
      
    ```
* Run ```mvn clean install```
* This will create a JAR suffixed with "-aws". This is what you would upload on AWS lambda. (This part I will add later on how to upload that in lambda.)


    
* If you are using the aws adapter the handler will always be: ```org.springframework.cloud.function.adapter.aws.FunctionInvoker::handleRequest```
